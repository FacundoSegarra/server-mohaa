FROM ubuntu:16.04

RUN dpkg --add-architecture i386
RUN apt-get clean
RUN apt-get update
RUN apt-get upgrade
RUN dpkg --configure -a

RUN apt-get install -f -y mailutils postfix curl netcat wget file tar bzip2 gzip unzip bsdmainutils python util-linux ca-certificates binutils bc jq tmux lib32gcc1 libstdc++6 lib32stdc++6 libstdc++5:i386

RUN groupadd -g 1000 mohaaserver && \
   useradd -r -u 1000 -g mohaaserver mohaaserver

WORKDIR /server

COPY . .

RUN chown -R mohaaserver:mohaaserver .
RUN chmod -R 777 /server/lgsm/
RUN chmod +x linuxgsm.sh

USER mohaaserver

ENV TERM xterm
RUN echo '55' | ./linuxgsm.sh

RUN ./mohaaserver auto-install

CMD ./mohaaserver start && tail -f /dev/null
EXPOSE 12203